module.exports = function(RED) {
    "use strict";
    var I2C = require("i2c-bus");

    // The Input Node
    function DI16IO1W(n) {

        RED.nodes.createNode(this, n);
        var node = this;
        var lastState = new Array(16);
        var dips = new Array(0x0100,0x0200,0x0400,0x0800,0x1000,0x2000,0x4000,0x8000,0x0080,0x0040,0x0020,0x0010,0x0008,0x0004,0x0002,0x0001);
        var dipshift = new Array(8,9,10,11,12,13,14,15,7,6,5,4,3,2,1,0);

        node.port = I2C.openSync( 0 );
        node.on("input", function(msg) {
            var address = 35;
            var command = 18;
            var buffcount = 2;
            var buffer = new Buffer(buffcount);

            node.port.readI2cBlock(address, command, buffcount, buffer, function(err, size, res) {
                if (err) {
                    node.error(err);
                } else {
                    var payload;
                    if (node.count == 1) {
                        payload = res[0];
                    } else {
                        payload = res;
                    }
                    
                    msg = Object.assign({}, msg);
                    //  node.log('log returned data'+  JSON.stringify([size, res.length, res, res.toString("utf-8")]));
                    msg.address = address;
                    msg.command = command;
                    msg.payload = payload;
                    msg.size    = size;

                    var msgs = new Array(16);
                    var dip;
                    var dipstate;

                    var sign = res[1] & (1 << 7);
                    var x = (((res[1] & 0xFF) << 8) | (res[0] & 0xFF));
                    dipstate = 0xFFFF0000 | x;  // fill in most significant bits with 1's

                    for(var i=0; i < dips.length; i++) {

                        dip = (dipstate&parseInt(dips[i]))>>dipshift[i];
                        if(dip!=0) {dip=0;} else {dip=1;}

                        if(dip==1 && lastState[i] != dip) msgs[i] = msg;

                        lastState[i] = dip;

                    }

                    node.send(msgs);
                }
            });
        });

        node.on("close", function() {
            node.port.closeSync();
        });
    }
    RED.nodes.registerType("16io1w inputs", DI16IO1W);

    // The Set Output Node
    function SDO16IO1W(n) {

        RED.nodes.createNode(this, n);
        this.digitalOutputs = n.digitalOutputs;
        var node = this;
        var dops = new Array(0x0080,0x0040,0x0020,0x0010,0x0008,0x0004,0x0002,0x0001,0x0080,0x0040,0x0020,0x0010,0x0008,0x0004,0x0002,0x0001);

        node.port = I2C.openSync( 0 );
        node.on("input", function(msg) {
            var address = 32;
            var command = 20;
            var buffcount = 2;
            var buffer = new Buffer(buffcount);

            node.port.readI2cBlock(address, command, buffcount, buffer, function(err, size, res) {
                if (err) {
                    node.error(err);
                } else {
                    var payload;
                    if (node.count == 1) {
                        payload = res[0];
                    } else {
                        payload = res;
                    }
                    
                    for (var i=0; i < node.digitalOutputs.length; i++) {

                        if(node.digitalOutputs[i].output <= 8) {
                            var bufferIndex = 1;
                        } else {
                            var bufferIndex = 0;
                        }

                        if(node.digitalOutputs[i].action == "on") res[bufferIndex] = res[bufferIndex] | parseInt(dops[node.digitalOutputs[i].output-1]);
                        else if(node.digitalOutputs[i].action == "off") res[bufferIndex] = res[bufferIndex] &~ parseInt(dops[node.digitalOutputs[i].output-1]);
                        else res[bufferIndex] = res[bufferIndex] ^ parseInt(dops[node.digitalOutputs[i].output-1]);

                    }

                    node.port.writeI2cBlock(address, command, res.length, res, function(err) {
                        if (err) {
                            node.error(err, msg);
                        } else {
                            node.send(msg);
                        };
                    });

                }
            });
        });

        node.on("close", function() {
            node.port.closeSync();
        });
    }
    RED.nodes.registerType("16io1w set outputs", SDO16IO1W);

    // The Get Output Node
    function GDO16IO1W(n) {

        RED.nodes.createNode(this, n);
        var node = this;
        var dops = new Array(0x0100,0x0200,0x0400,0x0800,0x1000,0x2000,0x4000,0x8000,0x0080,0x0040,0x0020,0x0010,0x0008,0x0004,0x0002,0x0001);
        var dopshift = new Array(8,9,10,11,12,13,14,15,7,6,5,4,3,2,1,0);

        node.port = I2C.openSync( 0 );
        node.on("input", function(msg) {
            var address = 32;
            var command = 20;
            var buffcount = 2;
            var buffer = new Buffer(buffcount);

            node.port.readI2cBlock(address, command, buffcount, buffer, function(err, size, res) {
                if (err) {
                    node.error(err);
                } else {
                    var payload;
                    if (node.count == 1) {
                        payload = res[0];
                    } else {
                        payload = res;
                    }
                    
                    msg = Object.assign({}, msg);
                    //  node.log('log returned data'+  JSON.stringify([size, res.length, res, res.toString("utf-8")]));
                    msg.address = address;
                    msg.command = command;
                    msg.payload = payload;
                    msg.size    = size;

                    var msgs = new Array(16);
                    var dop;
                    var dopstate;

                    var sign = res[1] & (1 << 7);
                    var x = (((res[1] & 0xFF) << 8) | (res[0] & 0xFF));
                    dopstate = 0xFFFF0000 | x;  // fill in most significant bits with 1's

                    for(var i=0; i < dops.length; i++) {

                        msg = Object.assign({}, msg);
                        msg.topic = "do" + (i+1);

                        dop = (dopstate&parseInt(dops[i]))>>dopshift[i];
                        if(dop!=0) {dop=1;} else {dop=0;}

                        if(dop==1) {
                            msg.payload = true;
                            msgs[i] = msg;
                        } else {
                            msg.payload = false;
                            msgs[i] = msg;
                        }

                    }

                    node.send(msgs);

                }
            });
        });

        node.on("close", function() {
            node.port.closeSync();
        });
    }
    RED.nodes.registerType("16io1w get outputs", GDO16IO1W);
    
}